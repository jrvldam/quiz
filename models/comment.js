// Definicion del mddelo de Comment con valizadion
module.exports = function(sequelize, DataType)
{
	return sequelize.define('Comment', {
		texto: {
			type: DataType.STRING,
			validate: { notEmpty: { msg: "-> Falta Comentario" } }
		},
		publicado: {
			type: DataType.BOOLEAN,
			defaultValue: false
		}
	});
};